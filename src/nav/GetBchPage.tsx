import * as React from 'react'
import NavFrame from './NavFrame';
import { SpacerRow } from '../wallet/components/SpacerRow';
import { Row, Col } from 'reactstrap';


interface Props { }

type AllProps = Props


export const GetBchPage = (props: AllProps) => {

  return (
    <NavFrame>
      <SpacerRow/>
      <h4>Get Bitcoin Cash</h4>
      <SpacerRow/>

      <Row>
        <Col xs="12">
          Need some Bitcoin Cash to get started?
        </Col>
      </Row>
      <SpacerRow/>
      <Row>
        <Col xs="12">
          You can buy from one of these sites and send it to your active address.
        </Col>
      </Row>

      <SpacerRow/>

      <Row>
        <Col xs="12">
          <a href="https://www.coinbase.com/">Coinbase</a>
        </Col>
      </Row>
      <Row>
        <Col xs="12">
          <a href="https://buy.bitcoin.com/">Bitcoin.com</a>
        </Col>
      </Row>
    </NavFrame>
  )
}
