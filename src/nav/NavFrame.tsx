import * as React from 'react';
import { useState } from 'react';
import { connect } from 'react-redux';
import { Navbar, NavbarBrand, Collapse, NavbarToggler, Nav, NavItem, Container } from "reactstrap";
import { NavLink } from 'react-router-dom';
import { NavState, NavItemDetails } from './types';


interface Props { 
  readonly children: JSX.Element[] | JSX.Element
}

interface PropsFromState { 
  readonly title?: string
  readonly logoImageSrc?: string
  readonly logoAltText?: string
  readonly logoWidth?: number
  readonly navItemDetails: NavItemDetails[]
  readonly activeNavItemIndex?: number
  readonly navItemStyle?: React.CSSProperties
  readonly activeNavItemStyle?: React.CSSProperties
}

interface PropsFromDispatch { }

type AllProps = PropsFromState &
  PropsFromDispatch &
  Props


const navDefaultLinkStyle: React.CSSProperties = {display: "block", padding: ".5rem 1rem"};
const navActiveLinkStyle: React.CSSProperties = {display: "block", padding: ".5rem 1rem", fontWeight: "bold"};
  

export const NavFrame = (props: AllProps) => {

  const [navbarOpen, setNavbarOpen] = useState(false);

  const getStyleWidth = (): number => {
    return 200;
  }
  
  const getImageNavbarBrand = () => {
    return <NavbarBrand href="/">
      <img src={props.logoImageSrc} 
        alt={props.logoAltText} 
        style={{width:props.logoWidth || getStyleWidth()}}
        />
    </NavbarBrand>
  }

  const getTextNavbarBrand = () => {
    return <NavbarBrand href="/">
      {props.title}
    </NavbarBrand>
  }

  const getNavbarBrand = () => {
    if (props.logoImageSrc && props.logoAltText) {
      return getImageNavbarBrand();
    }

    return getTextNavbarBrand();
  }

  const getLinkStyle = (navItemIndex: number): React.CSSProperties => {
    if (props.activeNavItemIndex && props.activeNavItemIndex === navItemIndex) {
      if (props.activeNavItemStyle) {
        return props.activeNavItemStyle
      } else {
        return navActiveLinkStyle;
      }
    }

    if (props.navItemStyle) {
      return props.navItemStyle
    }

    return navDefaultLinkStyle
  }


  return (
    <>
      <Navbar color="light" light={true} expand="md">
        {getNavbarBrand()}
        <NavbarToggler onClick={() => {setNavbarOpen(!navbarOpen) }} className="mr-2" />
        <Collapse isOpen={navbarOpen} navbar={true}>
          <Nav navbar={true}>
            {props.navItemDetails.map((d, index) => 
              <NavItem key={d.linkTo + d.title}>
                <NavLink to={d.linkTo} style={getLinkStyle(index)}>{d.title}</NavLink>
              </NavItem>)}
          </Nav>
        </Collapse>
      </Navbar>
      <Container>
        {props.children}
      </Container>
    </>
  )
}


const mapStateToProps = ({ nav }:{nav: NavState}): PropsFromState => {
  return {
    title: nav.title,
    logoImageSrc: nav.logoImageSrc,
    logoAltText: nav.logoAltText,
    logoWidth: nav.logoWidth,
    navItemDetails: nav.navItemDetails,
    activeNavItemIndex: nav.activeNavItemIndex,
    navItemStyle: nav.navItemStyle,
    activeNavItemStyle: nav.activeNavItemStyle,
  }
}


export default connect(
  mapStateToProps,
)(NavFrame)
