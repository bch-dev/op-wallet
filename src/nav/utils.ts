import { NavItemDetails } from "./types"
import { store } from "../OpWalletFrame"
import { setTitle, setLogoImageSrc, setLogoAltText, setBrandWidth, setNavItemDetails, setAboutPageNetworkSelectionEnabled } from "./navSlice"


export const configNavTitle = (title: string) => {
  store.dispatch(setTitle(title));
}


export const configNavImage = (imgSrc: string, imgAltText: string, imgWidth?: number) => {
  store.dispatch(setLogoImageSrc(imgSrc));
  store.dispatch(setLogoAltText(imgAltText));
  if (imgWidth) {
    store.dispatch(setBrandWidth(imgWidth));
  }
}


export const configNavLinks = (navItemDetails: NavItemDetails[]) => {
  store.dispatch(setNavItemDetails(navItemDetails));
}


export const configNavAboutPageNetworkSelectionEnabled = (enabled: boolean) => {
  store.dispatch(setAboutPageNetworkSelectionEnabled(enabled));
}
