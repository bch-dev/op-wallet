import * as React from 'react'
import NavFrame from './NavFrame';
import { getBbEnvForNetworkType, BchNetworkTypes } from 'bbnv';
import { SpacerRow } from '../wallet/components/SpacerRow';
import { Row, Col, Dropdown, Label, FormGroup, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux';
import { OpWalletState, getActiveWallet } from '..';
import { doSetActiveWallet, DEFAULT_MAINNET_WALLET_ID, DEFAULT_TESTNET_WALLET_ID } from '../wallet/walletSlice';


interface Props { }

type AllProps = Props


export const AboutPage = (props: AllProps) => {

  const dispatch = useDispatch();

  const activeWallet = useSelector((state: OpWalletState) => getActiveWallet(state));
  const isAboutPageNetworkSelectionEnabled = useSelector((state: OpWalletState) => state.nav.isAboutPageNetworkSelectionEnabled);


  const bbEnv = getBbEnvForNetworkType(activeWallet.walletNetworkType);;

  const [isNetworkChoiceDropdownOpen, setIsNetworkChoiceDropdownOpen] = React.useState(false);
  const toggle = () => setIsNetworkChoiceDropdownOpen(prevState => !prevState);


  const setNetworkType = (networkType: BchNetworkTypes) => {
    if (networkType === "mainnet") {
      dispatch(doSetActiveWallet({walletId: DEFAULT_MAINNET_WALLET_ID}))
    }

    if (networkType === "testnet") {
      dispatch(doSetActiveWallet({walletId: DEFAULT_TESTNET_WALLET_ID}))
    }
  }

  const getNetworkTypeLabel = () => {
    if (activeWallet.walletNetworkType=== "mainnet") {
      return "Use Mainnet";
    }

    return "Use Testnet";
  }


  return (
    <NavFrame>
      <SpacerRow/>
      <h4>Op-wallet Library</h4>
      <SpacerRow/>

      <Row>
        <Col xs="12">
          Op-wallet is a React library with domain-based wallet management functionality (provided by Bitbox) for quickly writing and deploying browser-based Bitcoin Cash apps.
        </Col>
      </Row>

      <SpacerRow/>

      <Row>
        <Col xs="12">
          WARNING: This library is a work in progress. DO NOT COMMIT NON-TRIVIAL AMOUNTS OF BCH TO THIS WALLET.
        </Col>
      </Row>

      <SpacerRow/>

      <Row>
        <Col xs="12">
          Parameters used in this this environment:
          <ul>
            <li>NetworkType: <b>{bbEnv.NetworkType}</b></li>
            <li>NetName: <b>{bbEnv.NetName}</b></li>
            <li>ExplorerUrlBase: <b>{bbEnv.ExplorerUrlBase}</b></li>
          </ul>
        </Col>
      </Row>

      {isAboutPageNetworkSelectionEnabled ? <>
        <SpacerRow/>

        <Row>
          <Col xs="12">
            <FormGroup>
              <Label for="networkChoice">Choose Bitcoin network</Label>
              <Dropdown isOpen={isNetworkChoiceDropdownOpen} toggle={toggle} id="networkChoice" >
                <DropdownToggle caret>
                  {getNetworkTypeLabel()}
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem onClick={() => setNetworkType("mainnet")}>Use Mainnet</DropdownItem>
                  <DropdownItem onClick={() => setNetworkType("testnet")}>Use Testnet</DropdownItem>
                </DropdownMenu>
              </Dropdown>
            </FormGroup>
          </Col>
        </Row>
      </> : <></>}

    </NavFrame>
  )
}
