
export interface NavItemDetails {
  readonly linkTo: string
  readonly title: string
}

export interface NavState {
  readonly title?: string
  readonly logoImageSrc?: string
  readonly logoAltText?: string
  readonly logoWidth?: number
  readonly navItemDetails: NavItemDetails[]
  readonly activeNavItemIndex?: number
  readonly navItemStyle?: React.CSSProperties
  readonly activeNavItemStyle?: React.CSSProperties
  readonly isAboutPageNetworkSelectionEnabled: boolean
}
