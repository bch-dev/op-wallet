import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { NavState, NavItemDetails } from './types'


const initialState: NavState = {
  title: "op-wallet",
  logoImageSrc: undefined,
  logoAltText: undefined,
  logoWidth: undefined,
  navItemDetails: [
    {
      linkTo: "/crypto",
      title: "Wallet"
    },
    {
      linkTo: "/about",
      title: "About"
    }
  ],
  isAboutPageNetworkSelectionEnabled: true
}


const navSlice = createSlice({
  name: 'nav',
  initialState,
  reducers: {

    setTitle(state, action: PayloadAction<string>) {
      state.title = action.payload;
    },

    setLogoImageSrc(state, action: PayloadAction<string>) {
      state.logoImageSrc = action.payload;
    },

    setLogoAltText(state, action: PayloadAction<string>) {
      state.logoAltText = action.payload;
    },

    setBrandWidth(state, action: PayloadAction<number>) {
      state.logoWidth = action.payload;
    },

    setNavItemDetails(state, action: PayloadAction<NavItemDetails[]>) {
      state.navItemDetails = action.payload;
    },

    setAboutPageNetworkSelectionEnabled(state, action: PayloadAction<boolean>) {
      state.isAboutPageNetworkSelectionEnabled = action.payload;
    }
  }
})


export const { setTitle, setLogoImageSrc, setLogoAltText, setBrandWidth, setNavItemDetails, setAboutPageNetworkSelectionEnabled } = navSlice.actions
export { initialState as initialNavState }

export default navSlice.reducer
