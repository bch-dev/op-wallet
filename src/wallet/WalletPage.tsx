import * as React from 'react'
import { FaucetComponent } from './components/FaucetComponent';
import { BalancesComponent } from './components/BalancesComponent';
import NavFrame from '../nav/NavFrame';
import { useSelector } from 'react-redux';
import { OpWalletState } from '../OpWalletFrame';
import { getActiveWallet } from './active';


interface Props { }

type AllProps = Props

export const WalletPage = (props: AllProps) => {

  const activeWallet = useSelector((state: OpWalletState) => getActiveWallet(state));

  return (
    <NavFrame>
      <BalancesComponent />
      <FaucetComponent
        activeWallet={activeWallet}
        />
    </NavFrame>
  )
}
