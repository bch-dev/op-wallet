import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { storeOpWallet, loadOpWallet } from './walletLocalStorage'
import { AppThunk } from '../OpWalletFrame'
import { BchNetworkTypes } from 'bbnv'
import { getNetworkTypeForAddress } from "bbnv/dist/opchop/utils";


export interface AddWalletRequest {
  networkType: BchNetworkTypes
}

export interface RemoveWalletRequest {
  walletId: string
}

export interface SetActiveWalletRequest {
  walletId: string
}

export interface RegisterAddressDataRequest {
  walletId: string
  label: string
  paymentKeyWif: string
  cashAddress: string
}

export interface DeleteAddressDataRequest {
  walletId: string
  cashAddressToDelete: string
}

export interface SetActiveAddressDataRequest {
  walletId: string
  activeCashAddress: string
}

export interface SetAddressLabelRequest {
  walletId: string
  cashAddress: string
  newLabel: string
}


export interface AddressData {
  walletId: string
  label: string
  paymentKeyWif: string
  cashAddress: string
  networkType: BchNetworkTypes
}


export interface Wallet {
  walletId: string
  walletNetworkType: BchNetworkTypes
  activeCashAddress?: string
  walletAddresses: AddressData[]
}

export interface OpWallet {
  activeWalletId: string
  wallets: Wallet[]
}

export const DEFAULT_MAINNET_WALLET_ID = "defaultMainnet";
export const DEFAULT_TESTNET_WALLET_ID = "defaultTestnet";


export const walletFactory = (walletId: string, networkType: BchNetworkTypes): Wallet => {
  return {
    walletId: walletId,
    walletNetworkType: networkType,
    activeCashAddress: undefined,
    walletAddresses: []
  }
}

const _defaultTestnetWallet = walletFactory(DEFAULT_TESTNET_WALLET_ID, "testnet");
const _defaultMainnetWallet = walletFactory(DEFAULT_MAINNET_WALLET_ID, "mainnet");

export const initialState: OpWallet = loadOpWallet({
  activeWalletId: _defaultMainnetWallet.walletId,
  wallets: [ 
    _defaultMainnetWallet,
    _defaultTestnetWallet,
  ]
});


const createNewWalletId = (existingWallets: string[]): string => {
  while (true) {
    const idInt = Math.round(Math.random() * 100000000)
    const idStr = "w" + idInt.toString();
    
    let idUnique = existingWallets.every(w => w !== idStr);
    if (idUnique) {
      return idStr;
    }
  }
}


const opWalletSlice = createSlice({
  name: 'opWallet',
  initialState,
  reducers: {

    addWallet(state, action: PayloadAction<AddWalletRequest>) {
      const walletIds = state.wallets.map(w => w.walletId);
      const newWalletId = createNewWalletId(walletIds);
      
      const newWallet = walletFactory(newWalletId, action.payload.networkType);
      state.wallets.push(newWallet);
    },

    removeWallet(state, action: PayloadAction<RemoveWalletRequest>) {
      if (action.payload.walletId === "defaultMainnet" || action.payload.walletId === "defaultTestnet") {
        return;
      }

      const removeWalletId = action.payload.walletId;
      const removeWalletIndex = state.wallets.findIndex(x => x.walletId === removeWalletId);
      if (removeWalletIndex === -1) {
        return;
      }

      const activeWalletIndex = state.wallets.findIndex(x => x.walletId === state.activeWalletId);
      if (activeWalletIndex === removeWalletIndex) {
        return;
      }

      const updatedWallets = state.wallets.filter(item => item.walletId !== removeWalletId);

      if (activeWalletIndex > removeWalletIndex) {
        const newActiveWalletIndex = activeWalletIndex - 1;
        state.activeWalletId = updatedWallets[newActiveWalletIndex].walletId;
      }

      state.wallets = updatedWallets;
    },

    setActiveWallet(state, action: PayloadAction<SetActiveWalletRequest>) {
      const activeWalletExists = state.wallets.some(w => w.walletId === action.payload.walletId);

      if (!activeWalletExists) {
        return;
      }

      const activeWalletIndex = state.wallets.findIndex(w => w.walletId === action.payload.walletId);
      const activeWallet = state.wallets[activeWalletIndex];

      state.activeWalletId = activeWallet.walletId;
    },

    registerAddressData(state, action: PayloadAction<RegisterAddressDataRequest>) {
      const walletForAddressIndex = state.wallets.findIndex(w => w.walletId === action.payload.walletId);
      if (walletForAddressIndex === -1) {
        return;
      }
      const walletForAddress = state.wallets[walletForAddressIndex];

      const newAddressNetworkType = getNetworkTypeForAddress(action.payload.cashAddress);
      if (walletForAddress.walletNetworkType !== newAddressNetworkType) {
        return;
      }

      const existingAddressIndex = walletForAddress.walletAddresses.findIndex(a => a.cashAddress === action.payload.cashAddress);      
      if (existingAddressIndex !== -1) {
        return;
      }

      const newAddressData: AddressData = {
        walletId: action.payload.walletId,
        label: action.payload.label,
        cashAddress: action.payload.cashAddress,
        paymentKeyWif: action.payload.paymentKeyWif,
        networkType: newAddressNetworkType
      }

      if (state.wallets[walletForAddressIndex].walletAddresses.length === 0) {
        state.wallets[walletForAddressIndex].activeCashAddress = newAddressData.cashAddress;
      }

      state.wallets[walletForAddressIndex].walletAddresses.push(newAddressData);
    },

    deleteAddressData(state, action: PayloadAction<DeleteAddressDataRequest>) {
      const walletForAddressIndex = state.wallets.findIndex(w => w.walletId === action.payload.walletId);
      if (walletForAddressIndex === -1) {
        return;
      }

      const walletForAddress = state.wallets[walletForAddressIndex];
      if (walletForAddress.activeCashAddress) {
        if (walletForAddress.activeCashAddress === action.payload.cashAddressToDelete) {
          return;
        }
      }

      const updatedAddresses = walletForAddress.walletAddresses.filter(a =>
        a.cashAddress !== action.payload.cashAddressToDelete);

      state.wallets[walletForAddressIndex].walletAddresses = updatedAddresses;
    },

    setActiveAddressData(state, action: PayloadAction<SetActiveAddressDataRequest>) {
      const walletForAddressIndex = state.wallets.findIndex(w => w.walletId === action.payload.walletId);
      if (walletForAddressIndex === -1) {
        return;
      }

      const walletForAddress = state.wallets[walletForAddressIndex];
      const addressToActivateIndex = walletForAddress.walletAddresses.findIndex(w => w.cashAddress === action.payload.activeCashAddress);
      const addressToActivate = walletForAddress.walletAddresses[addressToActivateIndex];

      state.wallets[walletForAddressIndex].activeCashAddress = addressToActivate.cashAddress;
    },

    setAddressDataLabel(state, action: PayloadAction<SetAddressLabelRequest>) {
      const walletForAddressIndex = state.wallets.findIndex(w => w.walletId === action.payload.walletId);
      if (walletForAddressIndex === -1) {
        return;
      }

      const walletForAddress = state.wallets[walletForAddressIndex];
      const addressToUpdateLabelIndex = walletForAddress.walletAddresses.findIndex(w => w.cashAddress === action.payload.cashAddress);

      state.wallets[walletForAddressIndex].walletAddresses[addressToUpdateLabelIndex].label = action.payload.newLabel;
    },
  }
})


export const { addWallet, removeWallet, setActiveWallet, registerAddressData, deleteAddressData, setActiveAddressData, setAddressDataLabel } = opWalletSlice.actions
export { initialState as initialWalletState }

export default opWalletSlice.reducer


export const doAddWallet = (
  request: AddWalletRequest,
): AppThunk => async (dispatch, getState) => {

  dispatch(addWallet(request))
  const s = getState();
  storeOpWallet(s.opWallet)
}


export const doRemoveWallet = (
  request: RemoveWalletRequest,
): AppThunk => async (dispatch, getState) => {

  dispatch(removeWallet(request))
  const s = getState();
  storeOpWallet(s.opWallet)
}


export const doSetActiveWallet = (
  request: SetActiveWalletRequest,
): AppThunk => async (dispatch, getState) => {

  dispatch(setActiveWallet(request))
  const s = getState();
  storeOpWallet(s.opWallet)
}


export const doRegisterAddressData = (
  request: RegisterAddressDataRequest
): AppThunk => async (dispatch, getState) => {

  dispatch(registerAddressData(request));
  const s = getState();
  storeOpWallet(s.opWallet);
}


export const doDeleteAddressData = (
  request: DeleteAddressDataRequest
): AppThunk => async (dispatch, getState) => {

  dispatch(deleteAddressData(request));
  const s = getState();
  storeOpWallet(s.opWallet);
}


export const doSetActiveAddressData = (
  request: SetActiveAddressDataRequest
): AppThunk => async (dispatch, getState) => {

  dispatch(setActiveAddressData(request));
  const s = getState();
  storeOpWallet(s.opWallet);
}


export const doSetAddressDataLabel = (
  request: SetAddressLabelRequest
): AppThunk => async (dispatch, getState) => {

  dispatch(setAddressDataLabel(request));
  const s = getState();
  storeOpWallet(s.opWallet);
}
