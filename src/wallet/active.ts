import { OpWalletState, Wallet, AddressData } from "..";

export const getActiveWallet = (ws: OpWalletState): Wallet => {
  const activeWalletIndex = ws.opWallet.wallets.findIndex(w => w.walletId === ws.opWallet.activeWalletId);
  const activeWallet = ws.opWallet.wallets[activeWalletIndex];

  return activeWallet;
}


export const getActiveAddressForWallet = (wallet: Wallet): AddressData | undefined => {
  const activeAddressIndex = wallet.walletAddresses.findIndex(a => a.cashAddress === wallet.activeCashAddress);
  
  return wallet.walletAddresses[activeAddressIndex];
}


export const getActiveAddress = (ws: OpWalletState): AddressData | undefined => {
  const activeWallet = getActiveWallet(ws);

  const activeAddress = getActiveAddressForWallet(activeWallet);

  return activeAddress;
}


export const getActivePaymentKey = (ws: OpWalletState): string => {
  const activeAddress = getActiveAddress(ws);
  if (activeAddress) {
    return activeAddress.paymentKeyWif;
  }

  return "";
}