import * as React from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Row, Col } from "reactstrap"
import QRCode from 'qrcode.react';
import { privateKeyToCashAddress } from "bbnv/dist/opchop/utils";
import { SpacerRow } from './SpacerRow';
import CopyToClipboard from 'react-copy-to-clipboard';
import { AddressData } from '../../wallet/walletSlice';


interface ReceiveBchModalProps {
  isOpen: boolean
  addressData: AddressData

  onClose: () => void
}

type AllProps = ReceiveBchModalProps


export const ReceiveBchModal = (props: AllProps) => {
  
  const addressData = privateKeyToCashAddress(props.addressData.paymentKeyWif);

  return (
    <Modal isOpen={props.isOpen} toggle={props.onClose} >
      <ModalHeader toggle={props.onClose}>Receive BCH</ModalHeader>
      <ModalBody>
        <Row>
          <Col xs="12" className="d-flex justify-content-center">
            <QRCode value={addressData} />
          </Col>
        </Row>
        <SpacerRow/>
        <Row>
          <Col xs="12">
            {addressData}
          </Col>
        </Row>
        <SpacerRow/>
        <Row>
          <Col xs="12">
            <CopyToClipboard
              text={addressData}
              >
              <Button block onClick={(e: { preventDefault: () => void; }) => {e.preventDefault(); }}>
                Copy to Clipboard
              </Button>
            </CopyToClipboard>
          </Col>
        </Row>
        <SpacerRow/>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.onClose}>Done</Button>{' '}
      </ModalFooter>
    </Modal>
  )
}