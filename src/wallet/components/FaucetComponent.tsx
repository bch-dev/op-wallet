import * as React from 'react'
import { MainnetFaucetComponent } from './MainnetFaucetComponent';
import { TestnetFaucetComponent } from './TestnetFaucetComponent';
import { BchImportComponent } from './BchImportComponent';
import { Wallet } from '../..';
import { Container } from 'reactstrap';


interface Props {
  activeWallet: Wallet
}


type AllProps = Props


export const FaucetComponent = (props: AllProps) => {

  switch (props.activeWallet.walletNetworkType) {
    case "mainnet": {
      return <Container>
        <MainnetFaucetComponent 
          activeWallet={props.activeWallet}
          />
        <BchImportComponent 
          activeWallet={props.activeWallet}
          />
      </Container>
    }
    case "testnet":
    default: {
      return <Container>
        <TestnetFaucetComponent 
          activeWallet={props.activeWallet}
          />
        <BchImportComponent 
          activeWallet={props.activeWallet}
          />
      </Container>
    }
  }
}
