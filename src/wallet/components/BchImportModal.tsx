import * as React from 'react'
import { useDispatch } from 'react-redux';
import { Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { SpacerRow } from './SpacerRow';
import { QrScannerInputBchPrivateKeyWif } from './QrScannerInputBchPrivateKeyWif';
import { Wallet } from '../..';
import { RegisterAddressDataRequest, doRegisterAddressData } from '../walletSlice';
import { privateKeyToCashAddress } from 'bbnv/dist/opchop/utils';


interface BchImportModalProps {
  isOpen: boolean
  activeWallet: Wallet

  onClose: () => void
}


type AllProps = BchImportModalProps


export const BchImportModal = (props: AllProps) => {

  const dispatch = useDispatch();

  const [keyToImport, setKeyToImport] = React.useState("");
  const [isValidKey, setIsValidKey] = React.useState(false);

  const networkType = props.activeWallet.walletNetworkType;

  const doImport = () => {
    const newAddressData: RegisterAddressDataRequest = {
      paymentKeyWif: keyToImport,
      label: "Imported @ " + new Date(),
      cashAddress: privateKeyToCashAddress(keyToImport),
      walletId: props.activeWallet.walletId
    }

    dispatch(doRegisterAddressData(newAddressData));

    props.onClose();
  }

  const onNewValidValue = (newValue: string) => {
    setIsValidKey(true);
    setKeyToImport(newValue);
  }

  const onNewInvalidValue = (newValue: string) => {
    setIsValidKey(false);
    setKeyToImport(newValue);
  }

  let label = "If you have a private key";
  if (networkType === "testnet") {
    label += " for testnet";
  }
  label += ", enter/scan it below.";


  return (
    <Modal isOpen={props.isOpen} toggle={props.onClose} >
      <ModalHeader toggle={props.onClose}>Import Private Key</ModalHeader>
      <ModalBody>
        <Row>
          <Col xs="12">
            {label}
          </Col>
        </Row>
        <SpacerRow/>
        <QrScannerInputBchPrivateKeyWif 
          onNewValidValue={onNewValidValue}
          onNewInvalidValue={onNewInvalidValue}
          />
        <Row>
          <Col xs="12">
            <Button block color="success"
              disabled={!isValidKey}
              onClick={() => doImport()}
              >
              Import
            </Button>
          </Col>
        </Row>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.onClose}>Done</Button>{' '}
      </ModalFooter>
    </Modal>
  )
}
