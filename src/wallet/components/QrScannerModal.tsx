import * as React from 'react'
import { Modal, ModalHeader, ModalBody, Row, Col, Alert, ModalFooter, Button } from "reactstrap"
import QrReader from 'react-qr-reader';
import { SpacerRow } from './SpacerRow';


export type PostScanCommand = "success-continue-scanning" | "invalid-continue-scanning" | "success-stop-scanning" | "invalid-stop-scanning"

export interface QrScanResult {
  postScanCommand: PostScanCommand
  scanStatus: string
}

interface QrScannerModalProps {
  isOpen: boolean
  title: string

  onScanData: (scanData: string) => void
  onClose: () => void
}

type AllProps = QrScannerModalProps


export const QrScannerModal = (props: AllProps) => {

  const [statusColor, setStatusColor] = React.useState("info");
  const [scanStatus, setScanStatus] = React.useState("Scanning");


  const handleScan = (data: string | null) => {
    if (data) {
      props.onScanData(data);
    }
  }
  
  const handleError = (err: any) => {
    console.error(err);
  }

  return (
    <Modal isOpen={props.isOpen} toggle={props.onClose} >
      <ModalHeader toggle={props.onClose}>{props.title}</ModalHeader>
      <ModalBody>
        <Row>
          <Col xs="12" className="d-flex justify-content-center">
            <QrReader
              delay={300}
              onError={handleError}
              onScan={handleScan}
              style={{ width: "50%" }}
              />
          </Col>
        </Row>
        <SpacerRow/>
        <Row>
          <Col xs="1"/>
          <Col xs="10">
            <Alert color={statusColor}>
              {scanStatus}
            </Alert>
          </Col>
          <Col xs="1"/>
        </Row>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.onClose}>Close</Button>{' '}
      </ModalFooter>
    </Modal>
  )
}
