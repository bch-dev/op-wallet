import * as React from 'react'
import { useState } from 'react';
import { Container, Row, Button, Col } from 'reactstrap';
import { useDispatch } from 'react-redux';
import { createPrivateKeyAndAddress, privateKeyToCashAddress } from 'bbnv/dist/opchop/utils';
import { doRegisterAddressData, Wallet, AddressData } from '../../wallet/walletSlice';
import { Link } from 'react-router-dom';


interface PropsFromState { 
  activeWallet: Wallet
}


type AllProps = PropsFromState  


export const MainnetFaucetComponent = (props: AllProps) => {

  const dispatch = useDispatch();

  const [addr, setAddr] = useState("xxaabb");
  
  const onNewWallet = () => {
    const {bchPrivateKey, bchCashAddress} = createPrivateKeyAndAddress("mainnet");

    const address: AddressData = {
      label: "My Address",
      paymentKeyWif: bchPrivateKey,
      cashAddress: privateKeyToCashAddress(bchPrivateKey),
      walletId: props.activeWallet.walletId,
      networkType: "mainnet",
    }

    dispatch(doRegisterAddressData(address));
    setAddr(bchCashAddress);
  };


  return (
    <>
      <Row>
        <Button color="link" onClick={(e: { preventDefault: () => void; }) => {e.preventDefault(); onNewWallet(); }}>
          Create a new address
        </Button>
      </Row>
      <Row>
        <Button color="link">
          <Link to="/get-bch" target='_blank'>Get Bitcoin Cash</Link>
        </Button>
      </Row>
    </>
  )
}
