import * as React from 'react'
import { useSelector } from 'react-redux'
import { NoActiveAddressNotificationComponent } from './NoActiveAddressNotificationComponent'
import { OpWalletState, getActiveAddress } from '../..'


interface AddressCheckerProps {
  readonly children: JSX.Element[] | JSX.Element
  readonly noAddressAvailableComponent?: JSX.Element
}

type AllProps = AddressCheckerProps


export const AddressChecker = (props: AllProps) => {

  const activeAddress = useSelector((state: OpWalletState) => getActiveAddress(state));

  if (!activeAddress) {
    if (props.noAddressAvailableComponent) {
      return (
        <>
          {props.noAddressAvailableComponent}
        </>
      )
    }

    return (
      <NoActiveAddressNotificationComponent />
    )
  }

  return (
    <>
      {props.children}
    </>
  )
}
