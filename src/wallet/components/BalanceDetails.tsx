import * as React from 'react'
import { Button, Card, CardHeader, CardTitle, CardBody, Row, Col, Collapse } from 'reactstrap';
import { useState, useEffect } from 'react';
import CopyToClipboard from 'react-copy-to-clipboard';
import { useDispatch, useSelector } from 'react-redux';
import { ReceiveBchModal } from './ReceiveBchModal';
// import { privateKeyToCashAddress, findBalance } from '../../opchop/utils';
import { privateKeyToCashAddress, findBalance } from 'bbnv/dist/opchop/utils'
import { SendBchModal } from './SendBchModal';
import { LabelAddressModal } from './LabelAddressModal';
import { ShowPrivateKeyModal } from './ShowPrivateKeyModal';
import { SpacerRow } from './SpacerRow';
import { ConfirmAddressDeletionModal } from './ConfirmAddressDeletionModal';
import { ReturnTestnetBchToFaucetModal } from './ReturnTestnetBchToFaucetModal';
import { doSetActiveAddressData, SetActiveAddressDataRequest, AddressData } from '../../wallet/walletSlice';
import { OpWalletState, getActiveWallet } from '../..';


interface Props {
  addressData: AddressData
  isActiveAddress: boolean
}


type AllProps = Props


interface CopyAddressProps {
  addr: string
}

export const CopyAddressButton = (props: CopyAddressProps ) => {
  const [addr, setAddr] = useState("ADDR NOT SET");

  const onClick = () => {
    setAddr(addr);
  };

  return (
    <CopyToClipboard
      text={props.addr}
      >
      <Button block onClick={(e: { preventDefault: () => void; }) => {e.preventDefault(); onClick(); }}>
        Copy
      </Button>
  </CopyToClipboard>
  )
}

export const BalanceDetails = (props: AllProps) => {

  const dispatch = useDispatch();
  const activeWallet = useSelector((state: OpWalletState) => getActiveWallet(state));

  const networkType = activeWallet.walletNetworkType;

  const [balance, setBalance] = useState(-1);
  const [isCollapsed, setIsCollapsed] = useState(true);

  const [isReceiveBchModalOpen, setIsReceiveBchModalOpen] = useState(false);
  const [isSendBchModalOpen, setIsSendBchModalOpen] = useState(false);
  const [isLabelAddressModalOpen, setIsLabelAddressModalOpen] = useState(false);
  const [isShowPrivateKeyModalOpen, setIsShowPrivateKeyModalOpen] = useState(false);
  const [isShowConfirmAddressDeletionModalOpen, setIsShowConfirmAddressDeletionModalOpen] = useState(false);
  const [isShowReturnTestnetBchToFaucetModalOpen, setIsShowReturnTestnetBchToFaucetModalOpen] = useState(false);

  const address = privateKeyToCashAddress(props.addressData.paymentKeyWif);

  const refreshBalance = async () => {
    try {
      const addressBalance = await findBalance(address);
        
      if (addressBalance !== balance) {
        console.log(addressBalance);
        setBalance(addressBalance);
      }

    } catch(error) {
      console.error(error)
    }
  }

  const setActive = async () => {
    const request: SetActiveAddressDataRequest = {
      activeCashAddress: props.addressData.cashAddress,
      walletId: props.addressData.walletId
    }
    dispatch(doSetActiveAddressData(request))
  }

  const getReturnTestnetBchToFaucetComponents = () => {
    return (
      <>
        <Button block color="danger" 
          onClick={() => setIsShowReturnTestnetBchToFaucetModalOpen(true)
          }>
            Return to Faucet
        </Button>
        <ReturnTestnetBchToFaucetModal
          isOpen={isShowReturnTestnetBchToFaucetModalOpen}
          addressData={props.addressData}
          onClose={() => 
            {
              setIsShowReturnTestnetBchToFaucetModalOpen(false);
              refreshBalance();
            }}
          />
      </>
    )
  }


  useEffect(() => { refreshBalance(); }, []);

  let labelToShow = address;
  if (props.addressData.label) {
    labelToShow = props.addressData.label;
  }


  return (
    <Row>
      <Col xs="12">
        <Card>
          <CardHeader>{props.isActiveAddress ? <b>{labelToShow}</b> : labelToShow}</CardHeader>
          <CardBody>
            <CardTitle>Balance: {balance} satoshis</CardTitle>

            <Row>
              <Col xs="6">
                <Button block onClick={refreshBalance}>Refresh Balance</Button> 
              </Col>
              <Col xs="6">
                <CopyAddressButton addr={address} />
              </Col>
            </Row>
            <SpacerRow paddingTop={5} paddingBottom={5} />
            <Row>
              <Col xs="6">
                <Button block onClick={setActive} disabled={props.isActiveAddress}>Set Active</Button> 
              </Col>
              <Col xs="6">
                <Button block onClick={() => setIsCollapsed(!isCollapsed)}>{isCollapsed ? "More" : "Less"}</Button> 
              </Col>
            </Row>
            
            <Collapse isOpen={!isCollapsed}>
              <SpacerRow />
              <Row>
                <Col xs="6">
                  <Button block color="info" onClick={() => setIsLabelAddressModalOpen(true)}>Label</Button>
                  <LabelAddressModal
                    isOpen={isLabelAddressModalOpen}
                    onClose={() => setIsLabelAddressModalOpen(false)}
                    addressData={props.addressData}
                    />
                </Col>
                <Col xs="6">
                  <Button block color="danger" onClick={() => setIsShowPrivateKeyModalOpen(true)}>Show Private Key</Button>
                    <ShowPrivateKeyModal
                      isOpen={isShowPrivateKeyModalOpen}
                      onClose={() => setIsShowPrivateKeyModalOpen(false)}
                      addressData={props.addressData}
                      />
                </Col>
              </Row>

              <SpacerRow paddingTop={5} paddingBottom={5} />

              <Row>
                <Col xs="6">
                  <Button block onClick={() => setIsReceiveBchModalOpen(true)}>Receive</Button>
                  <ReceiveBchModal
                    isOpen={isReceiveBchModalOpen}
                    onClose={() => setIsReceiveBchModalOpen(false)}
                    addressData={props.addressData}
                    />
                </Col>
                <Col xs="6">
                  <Button block color="danger"
                    disabled={props.isActiveAddress}
                    onClick={() => setIsShowConfirmAddressDeletionModalOpen(true)}
                    >
                    Delete Address
                  </Button>
                  <ConfirmAddressDeletionModal
                    isOpen={isShowConfirmAddressDeletionModalOpen}
                    onClose={() => setIsShowConfirmAddressDeletionModalOpen(false)}
                    addressData={props.addressData}
                    />
                </Col>
              </Row>

              <SpacerRow paddingTop={5} paddingBottom={5} />

              <Row>
                <Col xs="6">
                  <Button block onClick={() => setIsSendBchModalOpen(true)}>Send</Button>
                  <SendBchModal
                    isOpen={isSendBchModalOpen}
                    onClose={() => { 
                      setIsSendBchModalOpen(false);
                      refreshBalance();
                    }}
                    addressData={props.addressData}
                    balance={balance}
                    />
                </Col>
                <Col xs="6">
                  {networkType === "testnet" ? getReturnTestnetBchToFaucetComponents() : null}
                </Col>
              </Row>
            </Collapse>

          </CardBody>
        </Card>
      </Col>
    </Row>
  )
}
