import React, { useState } from "react"
import { QrScannerInput } from "./QrScannerInput"
import { Alert } from "reactstrap"
import { SpacerRow } from "./SpacerRow";
import { useSelector } from "react-redux";
import { OpWalletState, getActiveWallet } from "../..";
import { getNetworkTypeForAddress, getNetworkTypeForPaymentKey } from "bbnv/dist/opchop/utils";


export interface NewValueStatus {
  isValid: boolean
  newValue: string
  statusMessage: string
}


interface QrScannerInputBchPrivateKeyWifProps {
  onNewValidValue: (value: string) => void
  onNewInvalidValue: (value: string) => void
}

type AllProps = QrScannerInputBchPrivateKeyWifProps


export const QrScannerInputBchPrivateKeyWif = (props: AllProps) => {

  const activeWallet = useSelector((state: OpWalletState) => getActiveWallet(state));
  
  const [privateKeyWif, setPrivateKeyWif] = useState("");
  const [isValidKeyValue, setIsValidKeyValue] = useState(false);
  const [statusMessage, setStatusMessage] = useState("No key entered");


  const checkForInvalidKeyReason = (input: string): string => {

    try {
      const addressNetwork = getNetworkTypeForPaymentKey(input);
      if (activeWallet.walletNetworkType !== addressNetwork) {
        return "Address network type (" + addressNetwork + ") doesn't match wallet network type (" + activeWallet.walletNetworkType + ")";
      }

      setStatusMessage(`This wallet is on ${activeWallet.walletNetworkType}. Target address is for ${addressNetwork}.`);
    } catch (e) {
      console.log(e)
    }

    return "";
  }

  const checkForValidKey = (input: string): boolean => {
    return checkForInvalidKeyReason(input).length === 0;
  }

  const getStatusColor = (isValid: boolean): string => {
    return isValid ? "success" : "danger";
  }

  const onNewValue = (newKeyWif: string): void => {
    const invalidKeyReason = checkForInvalidKeyReason(newKeyWif);

    if (invalidKeyReason.length === 0) {
      props.onNewValidValue(newKeyWif);
      setPrivateKeyWif(newKeyWif);
      setStatusMessage("Valid key");
      setIsValidKeyValue(true);
    } else {
      props.onNewInvalidValue(newKeyWif);
      setStatusMessage("Invalid key value: " + invalidKeyReason);
      setIsValidKeyValue(false);
    }
  }
  

  return (
    <>
      <QrScannerInput
        scanQrTitle="Scan QR Key"
        checkScanDataValid={checkForValidKey}
        onUpdatedInput={onNewValue}
        forceQrScannerClosed={isValidKeyValue}
        />
      <SpacerRow/>
      <Alert color={getStatusColor(isValidKeyValue)}>
        {statusMessage}
      </Alert>
    </>
  )
}
