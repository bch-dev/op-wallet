import * as React from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Row, Col, Label, Alert } from "reactstrap"
import { SpacerRow } from './SpacerRow'
import { sendOpChopMessage } from "bbnv/dist/opchop/utils"
import { OpChopTxParameters } from 'bbnv/dist/opchop'
import { AddressData } from '../../wallet/walletSlice'


interface ReturnTestnetBchToFaucetModalProps {
  isOpen: boolean
  addressData: AddressData

  onClose: () => void
}


type AllProps = ReturnTestnetBchToFaucetModalProps


export const ReturnTestnetBchToFaucetModal = (props: AllProps) => {

  const [sendStatus, setSendStatus] = React.useState("");

  const doReturn = async () => {
    const BITCOIN_COM_FAUCET_RETURN_ADDRESS = "bchtest:qqmd9unmhkpx4pkmr6fkrr8rm6y77vckjvqe8aey35";
    
    const tx: OpChopTxParameters = {
      paymentKeyWif: props.addressData.paymentKeyWif,
      changeAddress: BITCOIN_COM_FAUCET_RETURN_ADDRESS,
    };

    setSendStatus("Sending");
    await sendOpChopMessage(tx);
    setSendStatus("Sent");
  }
  
  return (
    <Modal isOpen={props.isOpen} toggle={props.onClose} >
      <ModalHeader toggle={props.onClose}>Return BCH to Faucet</ModalHeader>
      <ModalBody>
        <Row>
          <Col xs="12" className="d-flex justify-content-center">
            <Label>Are you sure you want to return all BCH for this address back to a faucet?</Label>
          </Col>
        </Row>
        <SpacerRow />
        
        {sendStatus ? 
          <Row>
            <Col xs="12">
              <Alert color="info">
                {sendStatus}
              </Alert>
            </Col>
          </Row> : null
        }

        <Row>
          <Col xs="12">
            <Button block color="danger" 
              onClick={() => {
                doReturn();
                props.onClose();
              }}>
              Return It
            </Button>
          </Col>
        </Row>
        <SpacerRow />
        <Row>
          <Col xs="12">
            <Button block color="info" 
              onClick={() => props.onClose()
              }>
                Keep It... Don't Return
            </Button>
          </Col>
        </Row>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.onClose}>Close</Button>{' '}
      </ModalFooter>
    </Modal>
  )
}
