import * as React from 'react'
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Row, Col, Button } from 'reactstrap';
import CopyToClipboard from 'react-copy-to-clipboard'
import { createPrivateKeyAndAddress, privateKeyToCashAddress } from "bbnv/dist/opchop/utils";
import { Wallet, AddressData, doRegisterAddressData } from '../../wallet/walletSlice';


interface PropsFromState { 
  activeWallet: Wallet
}


type AllProps = PropsFromState


export const TestnetFaucetComponent = (props: AllProps) => {

  // navigate to https://developer.bitcoin.com/faucets/bch/

  const dispatch = useDispatch();

  const [copied, setCopied] = useState(false);
  const [addr, setAddr] = useState("");
  
  const onNewTestnetWallet = () => {
    const {bchPrivateKey, bchCashAddress} = createPrivateKeyAndAddress("testnet");

    const address: AddressData = {
      label: "My Address",
      paymentKeyWif: bchPrivateKey,
      cashAddress: privateKeyToCashAddress(bchPrivateKey),
      walletId: props.activeWallet.walletId,
      networkType: "testnet",
    }

    dispatch(doRegisterAddressData(address));
    setAddr(bchCashAddress);
  };

  const onCopy = () => {
    setCopied(true);
  };

  return (
    <Row>
      <Col>
        Need help getting started? Get some Bitcoin Cash to try it out!
        <br/>
        <ol>
          <li>
            <CopyToClipboard
              onCopy={onCopy}
              text={addr}
              >
              <Button color="link" onClick={(e: { preventDefault: () => void; }) => {e.preventDefault(); onNewTestnetWallet(); }}>
                Create a Testnet address.
              </Button>
            </CopyToClipboard>
          </li>
          <li>
            <Button color="link">
              <a target='_blank' rel="noopener noreferrer" href="https://developer.bitcoin.com/faucets/bch/">
                  Get Testnet BCH from a faucet.
              </a>
            </Button>
          </li>
        </ol>
      </Col>
    </Row>
  )
}
