import React, { useState } from "react"
import { QrScannerInput } from "./QrScannerInput"
import { Alert } from "reactstrap"
import { SpacerRow } from "./SpacerRow";
import { useSelector } from "react-redux";
import { OpWalletState, getActiveWallet } from "../..";
import { getNetworkTypeForAddress } from "bbnv/dist/opchop/utils";


export interface NewValueStatus {
  isValid: boolean
  newValue: string
  statusMessage: string
}


interface QrScannerInputBchAddressProps {
  onNewValidValue: (value: string) => void
  onNewInvalidValue: (value: string) => void
}

type AllProps = QrScannerInputBchAddressProps


export const QrScannerInputBchAddress = (props: AllProps) => {

  const activeWallet = useSelector((state: OpWalletState) => getActiveWallet(state));
  
  const [bchAddress, setBchAddress] = useState("");
  const [isValidAddressValue, setIsValidAddressValue] = useState(false);
  const [statusMessage, setStatusMessage] = useState("No address entered");


  const checkForValidAddress = (input: string): boolean => {
    
    try {
      const addressNetwork = getNetworkTypeForAddress(input);
      if (activeWallet.walletNetworkType === addressNetwork) {
        return true;
      }

      setStatusMessage(`This wallet is on ${activeWallet.walletNetworkType}. Target address is for ${addressNetwork}.`);
    } catch (e) {
      console.log(e)
    }

    return false;
  }

  const getStatusColor = (isValid: boolean): string => {
    return isValid ? "success" : "danger";
  }

  const onNewValue = (newAddress: string): void => {
    const isValidKey = checkForValidAddress(newAddress);

    if (isValidKey) {
      props.onNewValidValue(newAddress);
      setBchAddress(newAddress);
      setStatusMessage("Valid address");
      setIsValidAddressValue(true);

    } else {
      props.onNewInvalidValue(newAddress);
      setStatusMessage("Invalid address value");
      setIsValidAddressValue(false);
    }
  }
  

  return (
    <>
      <QrScannerInput
        scanQrTitle="Scan QR Key"
        checkScanDataValid={checkForValidAddress}
        onUpdatedInput={onNewValue}
        forceQrScannerClosed={isValidAddressValue}
        />
      <SpacerRow/>
      <Alert color={getStatusColor(isValidAddressValue)}>
        {statusMessage}
      </Alert>
    </>
  )
}