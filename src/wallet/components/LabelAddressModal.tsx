import * as React from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Row, Col, Label, Input } from "reactstrap"
import { useDispatch } from 'react-redux'
import { SpacerRow } from './SpacerRow'
import { SetAddressLabelRequest, doSetAddressDataLabel, AddressData } from '../../wallet/walletSlice'
import { privateKeyToCashAddress } from 'bbnv/dist/opchop/utils'


interface LabelAddressModalProps {
  isOpen: boolean
  addressData: AddressData

  onClose: () => void
}


type AllProps = LabelAddressModalProps


export const LabelAddressModal = (props: AllProps) => {

  const dispatch = useDispatch();

  const address = privateKeyToCashAddress(props.addressData.paymentKeyWif);

  const updateLabel = (newLabel: string) => {
    const request: SetAddressLabelRequest = {
      newLabel: newLabel,
      cashAddress: props.addressData.cashAddress,
      walletId: props.addressData.walletId
    }
    dispatch(doSetAddressDataLabel(request));
  }
  
  return (
    <Modal isOpen={props.isOpen} toggle={props.onClose} >
      <ModalHeader toggle={props.onClose}>Set Label for Address</ModalHeader>
      <ModalBody>
        <Row>
          <Col xs="12">
            You can show a name instead of a BCH address
          </Col>
        </Row>
        <Row>
          <Col xs="12">
            {address}
          </Col>
        </Row>
        <SpacerRow />
        <Row>
          <Col xs="12" className="d-flex justify-content-center">
            <Label for="addressLabel">Address Label</Label>
            <Input id="addressLabel"
              type="text"
              value={props.addressData.label} 
              onChange={e => updateLabel(e.target.value)}
              />
          </Col>
        </Row>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.onClose}>Done</Button>{' '}
      </ModalFooter>
    </Modal>
  )
}
