import * as React from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Row, Col } from "reactstrap"
import QRCode from 'qrcode.react';
import { AddressData } from '../../wallet/walletSlice';


interface ShowPrivateKeyModalProps {
  isOpen: boolean
  addressData: AddressData

  onClose: () => void
}

type AllProps = ShowPrivateKeyModalProps


export const ShowPrivateKeyModal = (props: AllProps) => (
  <Modal isOpen={props.isOpen} toggle={props.onClose} >
    <ModalHeader toggle={props.onClose}>Show Private Key</ModalHeader>
    <ModalBody>
      <Row>
        <Col xs="12" className="d-flex justify-content-center">
          <QRCode value={props.addressData.paymentKeyWif} />
        </Col>
      </Row>
    </ModalBody>
    <ModalFooter>
      <Button color="primary" onClick={props.onClose}>Done</Button>{' '}
    </ModalFooter>
  </Modal>
)
