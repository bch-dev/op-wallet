import * as React from 'react'
import { NavLink, Row, Col } from "reactstrap"
import { SpacerRow } from '../..'


interface NoActiveAddressNotificationComponentProps { }

type AllProps = NoActiveAddressNotificationComponentProps


export const NoActiveAddressNotificationComponent = (props: AllProps) => {

  return (
    <>
      <SpacerRow/>
      <h4>Address Required</h4>
      <SpacerRow/>
      <Row>
        <Col xs="12">
          <NavLink href="/crypto">Click here to create an address to continue.</NavLink>
        </Col>
      </Row>
    </>
  )
}
