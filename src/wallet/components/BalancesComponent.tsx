import * as React from 'react'
import { BalanceDetails } from './BalanceDetails';
import { useSelector } from 'react-redux';
import { SpacerRow } from './SpacerRow';
import { Fragment } from 'react';
import { OpWalletState } from '../../OpWalletFrame';
import { getActiveWallet, getActiveAddressForWallet } from '../active';


interface Props { }


type AllProps = Props


export const BalancesComponent = (props: AllProps) => {

  const activeWallet = useSelector((state: OpWalletState) => getActiveWallet(state));
  const activeAddress = getActiveAddressForWallet(activeWallet);

  const activeWalletAddresses = activeWallet.walletAddresses;

  let activeCashAddress: string | undefined = undefined;
  if (activeAddress) {
    activeCashAddress = activeAddress.cashAddress;
  }

  return (
    <>
      <SpacerRow/>
      <h4>My Addresses</h4>
      <SpacerRow/>
      
      {activeWalletAddresses.map(a => {
        return <Fragment key={a.paymentKeyWif}>
          <BalanceDetails
            addressData={a}
            isActiveAddress={a.cashAddress===activeCashAddress}
            />
          <SpacerRow />
        </Fragment>
      })}
    </>
  )
}
