import * as React from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Row, Col, Label } from "reactstrap"
import { useDispatch } from 'react-redux'
import { SpacerRow } from './SpacerRow'
import { doDeleteAddressData, DeleteAddressDataRequest, AddressData } from '../../wallet/walletSlice'


interface ConfirmAddressDeletionModalProps {
  isOpen: boolean
  addressData: AddressData

  onClose: () => void
}


type AllProps = ConfirmAddressDeletionModalProps


export const ConfirmAddressDeletionModal = (props: AllProps) => {

  const dispatch = useDispatch();

  const deleteAddress = async () => {
    const request: DeleteAddressDataRequest = {
      walletId: props.addressData.walletId,
      cashAddressToDelete: props.addressData.cashAddress,
    }
    dispatch(doDeleteAddressData(request));
  }
  
  return (
    <Modal isOpen={props.isOpen} toggle={props.onClose} >
      <ModalHeader toggle={props.onClose}>Set Label for Address</ModalHeader>
      <ModalBody>
        <Row>
          <Col xs="12" className="d-flex justify-content-center">
            <Label>Are you sure you want to delete this address?</Label>
          </Col>
        </Row>
        <SpacerRow />
        <Row>
          <Col xs="12">
            <Button block color="danger" 
              onClick={() => {
                deleteAddress();
                props.onClose();
              }}>
              Delete
            </Button>
          </Col>
        </Row>
        <SpacerRow />
        <Row>
          <Col xs="12">
            <Button block color="info" 
              onClick={() => props.onClose()
              }>
                Don't Delete
            </Button>
          </Col>
        </Row>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.onClose}>Close</Button>{' '}
      </ModalFooter>
    </Modal>
  )
}
