import * as React from 'react'
import { Input, Button, Row, Col } from "reactstrap"
import { QrScannerModal } from './QrScannerModal';


interface QrScannerInputProps {
  scanQrTitle: string
  forceQrScannerClosed: boolean

  checkScanDataValid: (qrData: string) => boolean
  onUpdatedInput: (input: string) => void
}

type AllProps = QrScannerInputProps


export const QrScannerInput = (props: AllProps) => {

  const [isQrScannerModalOpen, setIsQrScannerModalOpen] = React.useState(false);
  const [inputValue, setInputValue] = React.useState("");

  const updateInputValue = (updatedValue: string) => {
    setInputValue(updatedValue);
    props.onUpdatedInput(updatedValue);
  }

  const onScannedData = (qrData: string) => {
    updateInputValue(qrData);
  }

  if (isQrScannerModalOpen && props.forceQrScannerClosed) {
    setIsQrScannerModalOpen(false);
  }


  return (
    <Row>
      <Col xs="9">
        <Input 
          value={inputValue} 
          onChange={e => updateInputValue(e.target.value)}
          />
      </Col>
      <Col xs="3">
        <Button
          onClick={() => setIsQrScannerModalOpen(true)}
          >
            Scan
        </Button>
      </Col>
      <QrScannerModal 
        isOpen={isQrScannerModalOpen}
        title={props.scanQrTitle}
        onScanData={onScannedData}
        onClose={() => setIsQrScannerModalOpen(false)}
        />
    </Row>
  )
}