import * as React from 'react'
import { Row, Button } from 'reactstrap';
import { Wallet } from '../..';
import { BchImportModal } from './BchImportModal';


interface BchImportComponentProps {
  activeWallet: Wallet
}


type AllProps = BchImportComponentProps


export const BchImportComponent = (props: AllProps) => {

  const [isModalOpen, setIsModalOpen] = React.useState(false);

  return (
    <Row>
      <Button color="link" onClick={(e: { preventDefault: () => void; }) => {e.preventDefault(); setIsModalOpen(true); }}>
        Import a private key
      </Button>
      {isModalOpen ? 
        <BchImportModal
          activeWallet={props.activeWallet}
          isOpen={isModalOpen}
          onClose={() => setIsModalOpen(false)}
          /> : null }
    </Row>
  )
}
