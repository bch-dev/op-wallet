import * as React from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Row, Col, Label, Input, Alert } from "reactstrap"
import { privateKeyToCashAddress, sendOpChopMessage, estimateOpChopTxMinerFee, findBalance, DUST_LIMIT, getNetworkTypeForAddress } from "bbnv/dist/opchop/utils";
import { SpacerRow } from './SpacerRow';
import { QrScannerInputBchAddress } from './QrScannerInputBchAddress';
import { OpChopTxParameters } from 'bbnv/dist/opchop';
import { AddressData } from '../../wallet/walletSlice';
import { useSelector } from 'react-redux';
import { OpWalletState, getActiveWallet } from '../..';


interface SendBchModalProps {
  isOpen: boolean
  addressData: AddressData
  balance: number

  onClose: () => void
}

type AllProps = SendBchModalProps


export const SendBchModal = (props: AllProps) => {

  const activeWallet = useSelector((state: OpWalletState) => getActiveWallet(state));
  const networkType = activeWallet.walletNetworkType;
  
  const myAddress = privateKeyToCashAddress(props.addressData.paymentKeyWif);

  const [sendToAddress, setSendToAddress] = React.useState("");
  const [sendAmount, setSendAmount] = React.useState("");

  const [isInvalidAddress, setIsInvalidAddress] = React.useState(false);
  const [invalidSendAmountReason, setInvalidSendAmountReason] = React.useState("Balance amount not valid");

  const [validatedSendToAddress, setValidatedSendToAddress] = React.useState("");
  const [validatedSendToAmount, setValidatedSendToAmount] = React.useState(0);

  const [sendStatus, setSendStatus] = React.useState("");

  const [txFee, setTxFee] = React.useState(0);


  const updateSendToAddress = (updatedAddress: string) => {
    try {
      if (updatedAddress === myAddress) {
        throw new Error(`You can't send to the address you are sending from.`);
      }

      // const sendToAddrNetwork = BITBOX.Address.detectAddressNetwork(updatedAddress);
      const sendToAddrNetwork = getNetworkTypeForAddress(updatedAddress);
      if (sendToAddrNetwork !== networkType) {
        throw new Error(`This wallet is on ${networkType}. Target address is for ${sendToAddrNetwork}.`);
      }

      setValidatedSendToAddress(updatedAddress);
      setIsInvalidAddress(false);

    } catch (e) {
      setIsInvalidAddress(true);
    }

    setSendToAddress(updatedAddress);
  }

  const updateSendAmount = (updatedSendAmount: string) => {
    try {
      const sendAmountInt = parseInt(updatedSendAmount);
      if (Number.isNaN(sendAmountInt) || sendAmountInt < 1) {
        throw new Error("Balance amount not valid");
      }

      if (sendAmountInt < txFee) {
        throw new Error("Amount to send must be at least " + (txFee * 2));
      }

      if (sendAmountInt + txFee > props.balance) {
        throw new Error("Max available to send is " + (props.balance - txFee));
      }

      setValidatedSendToAmount(sendAmountInt);
      setInvalidSendAmountReason("");

    } catch (e) {
      setInvalidSendAmountReason(e.message);
    }
    
    setSendAmount(updatedSendAmount);
  }


  const doSend = async() => {
    const tx: OpChopTxParameters = {
      paymentKeyWif: props.addressData.paymentKeyWif,
      extraOutputs: [{
        address: validatedSendToAddress,
        amount: validatedSendToAmount
      }]
    };

    setSendStatus("Sending");
    await sendOpChopMessage(tx);
    setSendStatus("Sent");
  }

  const hasEnoughToSend = () => {
    return (props.balance - txFee) > DUST_LIMIT;
  }

  const getMaxSendAmount = () => {
    const maxAmount = props.balance - txFee;
    return maxAmount;
  }

  const setSendAmountMax = () => {
    const maxAmount = getMaxSendAmount();
    updateSendAmount(`${maxAmount}`);
  }

  const doClear = () => {
    setValidatedSendToAddress("");
    updateSendToAddress("");

    setValidatedSendToAmount(0);
    updateSendAmount("");
  }

  const getHeaderTitle = () => {
    if (hasEnoughToSend()) {
      return `Export BCH (up to ${getMaxSendAmount()})`
    }

    return "Export BCH (insufficient balance)";
  }

  const getInvalidAmountAlert = () => {
    if (isValidToSend) {
      return null;
    }

    return (
      <>
        {invalidSendAmountReason ? 
          <Row>
            <Col xs="12">
              <Alert color="danger">
                {invalidSendAmountReason}
              </Alert>
            </Col>
          </Row> : null
        }
        <SpacerRow />
      </>
    )
  }

  React.useEffect(() => {
    const getTxFee = async() => {
      if (props.balance < 1) {
        return;
      }
      
      const fee = await estimateOpChopTxMinerFee({
        paymentKeyWif: props.addressData.paymentKeyWif,
        extraOutputs: [{
          address: validatedSendToAddress,
          amount: validatedSendToAmount
        }]
      });
      setTxFee(fee.minerFee);
    }

    getTxFee();
  }, [props.balance])

  const isValidToSend = (!isInvalidAddress && !invalidSendAmountReason && hasEnoughToSend());


  return (
    <Modal isOpen={props.isOpen} toggle={props.onClose} >
      <ModalHeader toggle={props.onClose}>{getHeaderTitle()}</ModalHeader>
      <ModalBody>
        <Row>
          <Col xs="12">
            <Label for="sendToAddress">Export BCH To This Address</Label>
            <QrScannerInputBchAddress
              onNewValidValue={updateSendToAddress}
              onNewInvalidValue={updateSendToAddress}
              />
          </Col>
        </Row>
        <SpacerRow />
        <Row>
          <Col xs="12">
            <Label for="sendAmount">Export This Amount of BCH</Label>
            <Input id="sendAmount"
              type="text"
              value={sendAmount} 
              onChange={e => updateSendAmount(e.target.value)}
              />
          </Col>
        </Row>
        <SpacerRow />
        <Row>
          <Col xs="6">
            <Button block color="info" onClick={() => doClear()}>Clear</Button>
          </Col>
          <Col xs="6">
            <Button block color="info" onClick={() => setSendAmountMax()}>Export Max</Button>
          </Col>
        </Row>
        <SpacerRow />
        {getInvalidAmountAlert()}

        {sendStatus ? 
          <Row>
            <Col xs="12">
              <Alert color="info">
                {sendStatus}
              </Alert>
            </Col>
          </Row> : null
        }

        <Row>
          <Col xs="12">
            <Button block 
              color="warning" 
              onClick={() => doSend()}
              disabled={!isValidToSend}
              >
              Send
            </Button>
          </Col>
        </Row>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.onClose}>Close</Button>{' '}
      </ModalFooter>
    </Modal>
  )
}
