import { OpWallet, Wallet, AddressData } from "./walletSlice"
import { BchNetworkTypes } from "bbnv";


export interface AddressDataStorage {
  label: string
  paymentKeyWif: string
  cashAddress: string
}

export interface WalletStorage {
  walletId: string
  walletNetworkType: BchNetworkTypes
  activeAddressIndex: number
  walletAddresses: AddressDataStorage[]
}

export interface OpWalletStorage {
  activeWalletId: string
  wallets: WalletStorage[]
}


const OP_WALLET_SETTINGS_KEY = "OpWallet-Settings";


const mapAddressData = (addrData: AddressData): AddressDataStorage => {
  const addrDataStorage: AddressDataStorage = {
    cashAddress: addrData.cashAddress,
    label: addrData.label,
    paymentKeyWif: addrData.paymentKeyWif,
  }
  return addrDataStorage;
}

const mapWallet = (wallet: Wallet): WalletStorage => {
  const addrDataStorages = wallet.walletAddresses.map(a => mapAddressData(a));

  let activeAddressIndex = -1;
  if (wallet.activeCashAddress) {
    activeAddressIndex = wallet.walletAddresses.findIndex(x => x.cashAddress === wallet.activeCashAddress);
  }

  const wStorage: WalletStorage = {
    walletId: wallet.walletId,
    walletNetworkType: wallet.walletNetworkType,
    walletAddresses: addrDataStorages,
    activeAddressIndex
  }
  
  return wStorage;
}

const mapOpWallet = (opWallet: OpWallet): OpWalletStorage => {
  const wallets = opWallet.wallets.map(w => mapWallet(w));

  const opWalletStorage: OpWalletStorage = {
    activeWalletId: opWallet.activeWalletId,
    wallets,
  }

  return opWalletStorage;
}


const mapAddressDataStorage = (addrDataStorage: AddressDataStorage, walletStorage: WalletStorage): AddressData => {
  const addrData: AddressData = {
    walletId: walletStorage.walletId,
    cashAddress: addrDataStorage.cashAddress,
    label: addrDataStorage.label,
    paymentKeyWif: addrDataStorage.paymentKeyWif,
    networkType: walletStorage.walletNetworkType,
  }
  return addrData;
}

const mapWalletStorage = (walletStorage: WalletStorage): Wallet => {
  const addresses = walletStorage.walletAddresses.map((a, i) => mapAddressDataStorage(a, walletStorage));

  let activeCashAddress: string | undefined
  if (walletStorage.activeAddressIndex !== -1) {
    activeCashAddress = addresses[walletStorage.activeAddressIndex].cashAddress;
  }

  const wallet: Wallet = {
    walletId: walletStorage.walletId,
    walletNetworkType: walletStorage.walletNetworkType,
    walletAddresses: addresses,
    activeCashAddress: activeCashAddress
  }

  return wallet;
}

const mapOpWalletStorage = (opWalletStorage: OpWalletStorage): OpWallet => {
  const wallets = opWalletStorage.wallets.map(w => mapWalletStorage(w));
  
  const opWallet: OpWallet = {
    wallets,
    activeWalletId: opWalletStorage.activeWalletId
  }
  
  return opWallet;
}


export const storeOpWallet = (opwallet: OpWallet) => {
  const opWalletStorage = mapOpWallet(opwallet);
  const opWalletStr = JSON.stringify(opWalletStorage);
  localStorage.setItem(OP_WALLET_SETTINGS_KEY, opWalletStr);
}


export const loadOpWallet = (defaultOpWallet: OpWallet): OpWallet => {
  const opWalletStr = localStorage.getItem(OP_WALLET_SETTINGS_KEY);
  if (!opWalletStr) {
    return defaultOpWallet;
  }

  const opWalletStorage = JSON.parse(opWalletStr) as OpWalletStorage;
  const opWallet = mapOpWalletStorage(opWalletStorage);

  return opWallet;
}
