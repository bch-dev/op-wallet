import * as React from 'react'
import { combineReducers, Action } from 'redux'
import { Provider } from 'react-redux'
import { configureStore } from '@reduxjs/toolkit';
import { ThunkAction } from 'redux-thunk';
import { NavState } from './nav/types';
import walletReducer, { OpWallet, initialWalletState } from './wallet/walletSlice';
import navReducer, { initialNavState } from './nav/navSlice';


export interface OpWalletState {
  nav: NavState
  opWallet: OpWallet
}

export const defaultOpWalletState: OpWalletState = {
  nav: initialNavState,
  opWallet: initialWalletState,
}

export const rootReducer = combineReducers<OpWalletState>({
  nav: navReducer,
  opWallet: walletReducer,
})

export const store = configureStore({
  reducer: rootReducer,
  preloadedState: defaultOpWalletState,
})


interface OpWalletProps { 
  readonly children: JSX.Element[] | JSX.Element
}

export const OpWalletFrame = (props: OpWalletProps) => {

  return (
    <Provider store={store}>
      {props.children}
    </Provider>
  )
}


export type RootState = ReturnType<typeof rootReducer>
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>
