# op-wallet

Quickly write and deploy browser-based Bitcoin Cash apps with the **op-wallet** React library. It ships with several features designed to get your app's user interface out there quickly and painlessly.

* basic wallet management
* basic wallet user interface
* the ability to run on testnet or mainnet based on configuration
* integration with testnet and testnet faucets so that users of your app can get active quickly
* nav bar user interface for included components and your own

See [**op-wallet-memo-demo**](https://gitlab.com/bch-dev/op-wallet-memo-demo) for an example implementation.

To start your own project (assuming you have `create-react-app` installed - [see here if you don't](https://create-react-app.dev/docs/getting-started))...

* `npx create-react-app my-project-name --typescript`
* `npm install op-wallet`
* add to `src/index.tsx` of project: `import 'bootstrap/dist/css/bootstrap.min.css'`
* replace implementation of `App` function in `App.tsx` in created project with 
```
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { WalletPage, OpWalletFrame } from 'op-wallet'
...
...
const App = () => {
  return (
    <OpWalletFrame>
      <Router basename={process.env.PUBLIC_URL}>
        <Switch>
          <Route exact={true} path="/" component={WalletPage}/>
          <Route path="/crypto" component={WalletPage} />
        </Switch>
      </Router>
    </OpWalletFrame>
  )
}
```
* `npm run start`

Using GitLab CI and Pages, it's simple to host your project in a repo, publish it as a static web app, and make it publicly or privately accessible.
